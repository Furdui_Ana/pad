package md.utm.pad.labs.service;

import java.net.URI;

public interface LoadBalancerService {

    URI getServingNode();
}
