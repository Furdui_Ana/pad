package md.utm.pad.labs.exception;

public class UnknownHttpMethodException extends RuntimeException {
    public UnknownHttpMethodException(String message) {
        super(message);
    }
}
