package com.isa.pad.marketwarehouse.repository;

import com.isa.pad.marketwarehouse.model.Customer;
import com.isa.pad.marketwarehouse.model.OrderLine;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OrderLineRepository extends MongoRepository<OrderLine, String>{
}
